#include "king.h"
#define ONE_STEP 1

king::king(int color, char type) : gamePiece(color, type)
{
}

/*
	recieves the location of a piece (type: location), the location of the king (type: location) and the game (type:chessGame)
	checks if the movement is possible for the specific piece from the it's source to it's requested destination
	returns bool: true if valid, false if not valid
*/
bool king::condition(location& src, location& dest, chessGame& game)
{
	bool res = false;

	int x_src = src.getX();
	int y_src = src.getY();
	int x_dest = dest.getX();
	int y_dest = dest.getY();

	if ((x_dest == x_src + ONE_STEP && y_dest == y_src)						//one step right or
		|| (x_dest == x_src - ONE_STEP && y_dest == y_src)					//one step left or
		|| (y_dest == y_src + ONE_STEP && x_dest == x_src)					//one step up or
		|| (y_dest == y_src - ONE_STEP && x_dest == x_src)					//one step down or
		|| (x_dest == x_src + ONE_STEP && y_dest == y_src + ONE_STEP)		//one up and one right or
		|| (x_dest == x_src - ONE_STEP && y_dest == y_src + ONE_STEP)		//one up and one left or
		|| (x_dest == x_src + ONE_STEP && y_dest == y_src - ONE_STEP)		//one down and one right or
		|| (x_dest == x_src - ONE_STEP && y_dest == y_src - ONE_STEP))		//one down and one left
	{
		res = true;
	}
	return res;
}