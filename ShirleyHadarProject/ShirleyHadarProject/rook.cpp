#include "rook.h"

rook::rook(int color, char type) : gamePiece(color, type)
{
}

/*
	recieves the location of a piece (type: location), the location of the king (type: location) and the game (type:chessGame)
	checks if the movement is possible for the specific piece from the it's source to it's requested destination
	returns bool: true if valid, false if not valid
*/
bool rook::condition(location& src, location& dest, chessGame& game)
{
	bool res = false;

	int x_src = src.getX();
	int y_src = src.getY();
	int x_dest = dest.getX();
	int y_dest = dest.getY();

	// Either x doesn't change and y changes, or x changes and y doesn't change, and the way is clear.
	if ((((x_src == x_dest) && (y_src != y_dest)) || ((y_src == y_dest) && (x_src != x_dest))) && isWayClear(src, dest, game))						
	{
		res = true;
	}

	return res;
}

/*
	recieves the location of a piece (type: location), the location of the king (type: location) and the game (type:chessGame)
	checks if all the steps to the destination are clear
	returns bool: true if clear, flase if not
*/
bool rook::isWayClear(location& src, location& dest, chessGame& game)
{
	bool res = true;
	int i = 0;

	int x_src = src.getX();
	int y_src = src.getY();
	int x_dest = dest.getX();
	int y_dest = dest.getY();

	// Destination is to the right
	if (x_dest > x_src)
	{
		for (i = x_src + 1; i < x_dest && res; i++)
		{
			if (game.getBoard()->getPiece(i, y_src) != nullptr)
			{
				res = false;
			}
		}
	}

	//destination is to the left
	else if (x_dest < x_src)
	{
		for (i = x_dest + 1; i < x_src && res; i++)
		{
			if (game.getBoard()->getPiece(i, y_src) != nullptr)
			{
				res = false;
			}
		}
	}

	// Destination is down 
	else if (y_dest > y_src)
	{
		for (i = y_src + 1; i < y_dest && res; i++)
		{
			if (game.getBoard()->getPiece(x_src, i) != nullptr)
			{
				res = false;
			}
		}
	}

	//destination is up
	else if (y_dest < y_src)
	{
		for (i = y_dest + 1; i < y_src && res; i++)
		{
			if (game.getBoard()->getPiece(x_src, i) != nullptr)
			{
				res = false;
			}
		}
	}

	return res;
}