#include "knight.h"
#define ONE_STEP 1
#define TWO_STEPS 2

knight::knight(int color, char type) : gamePiece(color, type)
{
}

/*
	recieves the location of a piece (type: location), the location of the king (type: location) and the game (type:chessGame)
	checks if the movement is possible for the specific piece from the it's source to it's requested destination
	returns bool: true if valid, false if not valid
*/
bool knight::condition(location& src, location& dest, chessGame& game)
{
	bool res = false;

	int x_src = src.getX();
	int y_src = src.getY();
	int x_dest = dest.getX();
	int y_dest = dest.getY();

	if ((y_dest == y_src + TWO_STEPS && x_dest == x_src + ONE_STEP)				//two up and one right or
		|| (y_dest == y_src + TWO_STEPS && x_dest == x_src - ONE_STEP)			//two up and one left or
		|| (y_dest == y_src - TWO_STEPS && x_dest == x_src + ONE_STEP)			//two down and one right or
		|| (y_dest == y_src - TWO_STEPS && x_dest == x_src - ONE_STEP)			//two down and one left or
		|| (x_dest == x_src + TWO_STEPS && y_dest == y_src + ONE_STEP)			//two right and one up or
		|| (x_dest == x_src + TWO_STEPS && y_dest == y_src - ONE_STEP)			//two right and one down or
		|| (x_dest == x_src - TWO_STEPS && y_dest == y_src + ONE_STEP)			//two left and one up or
		|| (x_dest == x_src - TWO_STEPS && y_dest == y_src - ONE_STEP))			//two left and one down
	{
		res = true;
	}

	return res;
}