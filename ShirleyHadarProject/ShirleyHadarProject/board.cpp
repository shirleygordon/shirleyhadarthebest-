#include "board.h"
#include "bishop.h"
#include "king.h"
#include "knight.h"
#include "pawn.h"
#include "rook.h"
#include "queen.h"

/*
Default C'tor for board object.
Input: none.
Output: none.
*/
board::board()
{
	unsigned int i = 0, j = 0;

	// Set white pieces
	_board[WHITE_ROW][ROOK1] = new rook(WHITE, ROOK);
	_board[WHITE_ROW][KNIGHT1] = new knight(WHITE, KNIGHT);
	_board[WHITE_ROW][BISHOP1] = new bishop(WHITE, BISHOP);
	_board[WHITE_ROW][KING] = new king(WHITE, KING_CHAR);
	_board[WHITE_ROW][QUEEN] = new queen(WHITE, QUEEN_CHAR);
	_board[WHITE_ROW][BISHOP2] = new bishop(WHITE, BISHOP);
	_board[WHITE_ROW][KNIGHT2] = new knight(WHITE, KNIGHT);
	_board[WHITE_ROW][ROOK2] = new rook(WHITE, ROOK);
	
	// Set black pieces
	_board[BLACK_ROW][ROOK1] = new rook(BLACK, ROOK);
	_board[BLACK_ROW][KNIGHT1] = new knight(BLACK, KNIGHT);
	_board[BLACK_ROW][BISHOP1] = new bishop(BLACK, BISHOP);
	_board[BLACK_ROW][KING] = new king(BLACK, KING_CHAR);
	_board[BLACK_ROW][QUEEN] = new queen(BLACK, QUEEN_CHAR);
	_board[BLACK_ROW][BISHOP2] = new bishop(BLACK, BISHOP);
	_board[BLACK_ROW][KNIGHT2] = new knight(BLACK, KNIGHT);
	_board[BLACK_ROW][ROOK2] = new rook(BLACK, ROOK);

	// Set pawns
	for (i = 0; i < COLS; i++)
	{
		_board[WHITE_PAWN_ROW][i] = new pawn(WHITE, PAWN);
		_board[BLACK_PAWN_ROW][i] = new pawn(BLACK, PAWN);
	}

	// Set rest of the board to nullptr
	for (i = BLACK_PAWN_ROW + 1; i < WHITE_PAWN_ROW; i++)
	{
		for (j = 0; j < COLS; j++)
		{
			_board[i][j] = nullptr;
		}
	}
}

/*
D'tor for board.
Input: none.
Output: none.
*/
board::~board()
{
	unsigned int i = 0, j = 0;

	for (i = 0; i < ROWS; i++)
	{
		for (j = 0; j < COLS; j++)
		{
			if (_board[i][j] != nullptr) // If location is not empty
			{
				delete _board[i][j]; // Delete pointer
			}
		}
	}
}

/*
Function updates the board according to the move request.
Input: move request.
Output: none.
*/
void board::updateBoard(moveRequest moveReq)
{
	setPiece(moveReq.getDest(), this->getPiece(moveReq.getSrc())); // Move piece to its destination
	_board[moveReq.getSrc().getY()][moveReq.getSrc().getX()] = nullptr; // Make the source location a nullptr
}

/*
Function returns a pointer to the piece in the specified location.
Input: location.
Output: gamePiece pointer.
*/
gamePiece* board::getPiece(location srcLoc)
{
	return _board[srcLoc.getY()][srcLoc.getX()];
}

/*
Function returns a pointer to the piece in the specified coordinates.
Input: x and y coordinates.
Output: gamePiece pointer.
*/
gamePiece* board::getPiece(int x, int y)
{
	return _board[y][x];
}

/*
Function puts the new piece in the specified location on the board.
Input: location, new piece pointer.
Output: none.
*/
void board::setPiece(location srcLoc, gamePiece* newPiece)
{
	delete _board[srcLoc.getY()][srcLoc.getX()]; // Delete old piece in that location
	_board[srcLoc.getY()][srcLoc.getX()] = newPiece; // Put the new piece in the location
}
