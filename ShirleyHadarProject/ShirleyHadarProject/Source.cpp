/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <thread>
#include "stdafx.h"

using std::cout;
using std::endl;
using std::string;


void main()
{
	chessGame game;
	game.getBoard();
	srand(time_t(NULL));
	
	Pipe p;
	bool isConnect = p.connect();
	
	string ans;

	while (!isConnect)
	{
		cout << "Can't connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	

	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1"); // just example...
	
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		game.updateMoveRequest(msgFromGraphics); // Update the move request with new request
		game.movePiece(); // Attempt to move the piece

		strcpy(msgToGraphics, game.getMoveReq().getAnswer().c_str()); // Copy the answer string into the msgToGraphics
		
		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
}