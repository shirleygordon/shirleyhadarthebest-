#pragma once

#define LETTER_TO_NUM 97
#define ASCII_TO_INT 49
#define COLS 7

class location
{
private:
	unsigned int _x;
	unsigned int _y;

public:
	location();
	~location();

	unsigned int getX();
	unsigned int getY();

	void setX(char x);
	void setY(char y);
	void setLocation(unsigned int x, unsigned int y);
};
