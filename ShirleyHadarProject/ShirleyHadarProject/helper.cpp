#include "helper.h"

/*
Function checks if the player requested to move to a location outside the board.
Input: game reference.
Output: none.
*/
void helper::isOutOfBounds(chessGame& game)
{
	location srcLoc = game.getMoveReq().getSrc();
	location destLoc = game.getMoveReq().getDest();

	// If the x or y aren't between 0-7, throw exception.
	if (srcLoc.getX() < 0 || srcLoc.getX() >= COLS || srcLoc.getY() < 0 || srcLoc.getY() >= ROWS)
	{
		throw answerCodes::OUT_OF_BOUNDS; // If source location is out of bounds.
	}
	else if (destLoc.getX() < 0 || destLoc.getX() >= COLS || destLoc.getY() < 0 || destLoc.getY() >= ROWS)
	{
		throw answerCodes::OUT_OF_BOUNDS; // If destination location is out of bounds.
	}
}

/*
Function checks if the source location contains a chess piece that
belongs to the current player.
Input: game reference.
Output: none.
*/
void helper::isPlayerInSrcLocation(chessGame& game)
{
	location srcLoc = game.getMoveReq().getSrc();
	
	// If source location is empty or contains an enemy's piece, throw error code.
	if (game.getBoard()->getPiece(srcLoc) == nullptr || game.getBoard()->getPiece(srcLoc)->getColor() != game.getCurrentPlayer())
	{
		throw answerCodes::NO_PLAYER_IN_SRC;
	}
}

/*
Function checks if the destination location contains a chess piece that
belongs to the current player.
Input: game reference.
Output: none.
*/
void helper::isPlayerInDestLocation(chessGame& game)
{
	location destLoc = game.getMoveReq().getDest();

	// If source location is not empty and contains the current player's piece, throw error code.
	if (game.getBoard()->getPiece(destLoc) != nullptr && game.getBoard()->getPiece(destLoc)->getColor() == game.getCurrentPlayer())
	{
		throw answerCodes::PLAYER_IN_DEST;
	}
}

/*
Function checks if the move led the enemy to be checked.
Input: chessGame reference.
Output: none.
*/
void helper::isEnemyChecked(chessGame& game)
{
	bool isEnemyChecked = false;
	location srcLoc;
	location enemyKingLocation;
	unsigned int i = 0, j = 0;

	enemyKingLocation = findEnemyKing(game);

	// Check if the piece the player moved put the enemy in check.
	//isEnemyChecked = game.getBoard()->getPiece(game.getMoveReq().getSrc())->canMoveToKing(game.getMoveReq().getDest(), enemyKingLocation, game);

	// Go over the board, if the piece in the current location is mine check if it can
	// move to the enemy king's location.
	for (i = 0; i < ROWS && !isEnemyChecked; i++)
	{
		for (j = 0; j < COLS && !isEnemyChecked; j++)
		{
			if (game.getBoard()->getPiece(i, j) != nullptr && game.getBoard()->getPiece(i, j)->getColor() == game.getCurrentPlayer()) // Only check if pieces of the current player can move to enemy king
			{
				srcLoc.setLocation(i, j);
				isEnemyChecked = game.getBoard()->getPiece(srcLoc)->canMoveToKing(srcLoc, enemyKingLocation, game); // Check if the piece can move to the kings location
			}
		}
	}

	if (isEnemyChecked)
	{
		throw answerCodes::ENEMY_CHECKED;
	}
}

/*
Function find the location of the enemy king.
Input: reference to chessGame.
Output: location of enemy king.
*/
location helper::findEnemyKing(chessGame& game)
{
	bool enemyKingFound = false;
	//char board[ROWS][COLS] = { 0 };
	gamePiece* currentPiece = nullptr;
	location enemyKingLocation;
	unsigned int i = 0, j = 0;

	// Copy board into a new variable
	//std::copy(&game.getBoard()[0][0], &game.getBoard()[0][0] + ROWS * COLS, &board[0][0]);

	// Go over every location on the board, if the location contains the enemy king,
	// save its location.
	for (i = 0; i < ROWS && !enemyKingFound; i++)
	{
		for (j = 0; j < COLS && !enemyKingFound; j++)
		{
			// If the current piece is a king
			if (game.getBoard()->getPiece(i, j) != nullptr && game.getBoard()->getPiece(i, j)->getType() == KING_CHAR)
			{
				// Current player is white and the king found is black
				if (!game.getCurrentPlayer() && game.getBoard()->getPiece(i, j)->getColor() == BLACK)
				{
					enemyKingFound = true;
					enemyKingLocation.setLocation(i, j);
				}
				// Current player is black and the king found is white
				else if (game.getCurrentPlayer() && game.getBoard()->getPiece(i, j)->getColor() == WHITE) 
				{
					enemyKingFound = true;
					enemyKingLocation.setLocation(i, j);
				}
			}
		}
	}

	return enemyKingLocation;
}

/*
Function checks if the current piece is a threatening queen or rook
and changes the parameters accordingly.
Input: game, isChecked, exitLoop, currentPieceType and currentPieceColor references.
Output: none.
*/
void helper::threateningQueenOrRook(chessGame& game, bool& isChecked, bool& exitLoop, char& currentPieceType, unsigned int& currentPieceColor)
{
	// If there's an enemy queen or rook in the same row/column as king and no protectiong piece before it - the king is in check..
	if (currentPieceColor != game.getCurrentPlayer() && (currentPieceType == QUEEN_CHAR || currentPieceType == ROOK))
	{
		isChecked = true;
	}
	// Otherwise, there's a piece protecting the king, exit the loop (if the piece isn't an enemy queen or rook, it must be a piece protecting the king).
	else
	{
		exitLoop = true;
	}
}

/*
Function checks if the current piece is a threatening bishop or queen
and changes the parameters accordingly.
Input: game, isChecked, exitLoop, currentPieceType and currentPieceColor references.
Output: none.
*/
void helper::threateningBishopOrQueen(chessGame& game, bool& isChecked, bool& exitLoop, char& currentPieceType, unsigned int& currentPieceColor)
{
	// If there's an enemy queen or rook in the same diagonal as king and no protectiong piece before it - the king is in check.
	if (currentPieceColor != game.getCurrentPlayer() && (currentPieceType == BISHOP || currentPieceType == QUEEN_CHAR))
	{
		isChecked = true;
	}
	// Otherwise, there's a piece protecting the king, exit the loop (if the piece isn't an enemy queen or rook, it must be a piece protecting the king).
	else
	{
		exitLoop = true;
	}
}

/*
Function checks the kings' row & column for threatening rooks & queens.
Input: game reference, myKingLocation reference, isChecked reference, exitLoop reference.
Output: none.
*/
void helper::checkRowAndColumn(chessGame& game, location& myKingLocation, bool& isChecked, bool& exitLoop)
{
	location moveReqSrc = game.getMoveReq().getSrc();
	location moveReqDest = game.getMoveReq().getDest();
	unsigned int currentPieceColor = 0;
	char currentPieceType = 0;
	int i = 0;

	// Check king's column
	// Up compared to king (check from the place closest to the king).
	// Only check if king's y isn't 0 (as in that case there won't be any piece upwards from it).
	if (myKingLocation.getY() > 0)
	{
		for (i = myKingLocation.getY() - 1; i >= 0 && !isChecked && !exitLoop; i--)
		{
			// If the current location isn't empty and isn't the source location of the move request (as it will be empty after the move)
			if (game.getBoard()->getPiece(myKingLocation.getX(), i) != nullptr && !(myKingLocation.getX() == moveReqSrc.getX() && i == moveReqSrc.getY()))
			{
				// Get current piece type and color.
				currentPieceType = game.getBoard()->getPiece(myKingLocation.getX(), i)->getType();
				currentPieceColor = game.getBoard()->getPiece(myKingLocation.getX(), i)->getColor();

				// Check if there's a threatening queen or rook in the current location.
				threateningQueenOrRook(game, isChecked, exitLoop, currentPieceType, currentPieceColor);
			}
			// If the current location is the destination location of the piece to move
			if (myKingLocation.getX() == moveReqDest.getX() && i == moveReqDest.getY())
			{
				isChecked = false;
				exitLoop = true; // The piece will be protecting the king, so exit the loop.
			}
		}
	}

	exitLoop = isChecked; // If king is checked, there's no need to keep checking.

	// Down compared to king (check from the place closest to the king)
	for (i = myKingLocation.getY() + 1; i < COLS && !isChecked && !exitLoop; i++)
	{
		// If the current location isn't empty and isn't the source location of the move request (as it will be empty after the move)
		if (game.getBoard()->getPiece(myKingLocation.getX(), i) != nullptr && !(myKingLocation.getX() == moveReqSrc.getX() && i == moveReqSrc.getY()))
		{
			currentPieceType = game.getBoard()->getPiece(myKingLocation.getX(), i)->getType();
			currentPieceColor = game.getBoard()->getPiece(myKingLocation.getX(), i)->getColor();

			// Check if there's a threatening queen or rook in the current location.
			threateningQueenOrRook(game, isChecked, exitLoop, currentPieceType, currentPieceColor);
		}
		// If the current location is the destination location of the piece to move
		if (myKingLocation.getX() == moveReqDest.getX() && i == moveReqDest.getY())
		{
			isChecked = false;
			exitLoop = true; // The piece will be protecting the king, so exit the loop.
		}
	}

	exitLoop = isChecked; // If king is checked, there's no need to keep checking.

	// Check kings row
	// Left side (check from the place closest to the king)
	// Only check if king's x isn't 0 (as in that case there won't be any piece to its left).
	if (myKingLocation.getX() != 0)
	{
		for (i = myKingLocation.getX() - 1; i < myKingLocation.getX() && !isChecked && !exitLoop; i--)
		{
			// If the current location isn't empty and isn't the source location of the move request (as it will be empty after the move)
			if (game.getBoard()->getPiece(i, myKingLocation.getY()) != nullptr && !(i == moveReqSrc.getX() && myKingLocation.getY() == moveReqSrc.getY()))
			{
				currentPieceType = game.getBoard()->getPiece(i, myKingLocation.getY())->getType();
				currentPieceColor = game.getBoard()->getPiece(i, myKingLocation.getY())->getColor();

				// Check if there's a threatening queen or rook in the current location.
				threateningQueenOrRook(game, isChecked, exitLoop, currentPieceType, currentPieceColor);
			}
			// If the current location is the destination location of the piece to move
			if (i == moveReqDest.getX() && myKingLocation.getY() == moveReqDest.getY())
			{
				isChecked = false;
				exitLoop = true; // The piece will be protecting the king, so exit the loop.
			}
		}
	}

	exitLoop = isChecked; // If king is checked, there's no need to keep checking.

	// Right side (check from the place closest to the king)
	for (i = myKingLocation.getX() + 1; i < ROWS && !isChecked && !exitLoop; i++)
	{
		// If the current location isn't empty and isn't the source location of the move request (as it will be empty after the move)
		if (game.getBoard()->getPiece(i, myKingLocation.getY()) != nullptr && !(i == moveReqSrc.getX() && myKingLocation.getY() == moveReqSrc.getY()))
		{
			currentPieceType = game.getBoard()->getPiece(i, myKingLocation.getY())->getType();
			currentPieceColor = game.getBoard()->getPiece(i, myKingLocation.getY())->getColor();

			// Check if there's a threatening queen or rook in the current location.
			threateningQueenOrRook(game, isChecked, exitLoop, currentPieceType, currentPieceColor);
		}
		// If the current location is the destination location of the piece to move
		if (i == moveReqDest.getX() && myKingLocation.getY() == moveReqDest.getY())
		{
			isChecked = false;
			exitLoop = true; // The piece will be protecting the king, so exit the loop.
		}
	}

	exitLoop = isChecked; // If king is checked, there's no need to keep checking.
}

/*
Function checks the kings' diagonals for threatening bishops & queens.
Input: game reference, myKingLocation reference, isChecked reference, exitLoop reference.
Output: none.
*/
void helper::checkDiagonals(chessGame& game, location& myKingLocation, bool& isChecked, bool& exitLoop)
{
	location moveReqSrc = game.getMoveReq().getSrc();
	location moveReqDest = game.getMoveReq().getDest();
	unsigned int currentPieceColor = 0;
	char currentPieceType = 0;
	int i = 0, j = 0;

	// Check first diagonal
	// Up-left (check from the place closest to the king).
	// Only check if king's location isn't the top-left corner.
	if (!(myKingLocation.getX() == 0 && myKingLocation.getY() == 0))
	{
		for (i = myKingLocation.getX() - 1; i >= 0 && !isChecked && !exitLoop; i--)
		{
			for (j = myKingLocation.getY() - 1; j >= 0 && !isChecked && !exitLoop; j--)
			{
				// Only continue checking if the current location is part of the king's diagonal.
				if ((myKingLocation.getX() - i) == (myKingLocation.getY() - j))
				{
					// If the current location isn't empty and isn't the source location of the move request (as it will be empty after the move)
					if (game.getBoard()->getPiece(i, j) != nullptr && !(i == moveReqSrc.getX() && j == moveReqSrc.getY()))
					{
						// Get current piece type and color.
						currentPieceType = game.getBoard()->getPiece(i, j)->getType();
						currentPieceColor = game.getBoard()->getPiece(i, j)->getColor();

						// Check if there's a threatening queen or rook in the current location.
						threateningBishopOrQueen(game, isChecked, exitLoop, currentPieceType, currentPieceColor);
					}
					// If the current location is the destination location of the piece to move
					if (i == moveReqDest.getX() && j == moveReqDest.getY())
					{
						isChecked = false;
						exitLoop = true; // The piece will be protecting the king, so exit the loop.
					}
				}
			}
			
		}
	}

	exitLoop = isChecked; // If king is checked, there's no need to keep checking.

	// Down-right (check from the place closest to the king).
	// Only check if king's location isn't the bottom-right corner. 
	if (!(myKingLocation.getX() == CORNER && myKingLocation.getY() == CORNER))
	{
		for (i = myKingLocation.getX() + 1; i < ROWS && !isChecked && !exitLoop; i++)
		{
			for (j = myKingLocation.getY() + 1; j < COLS && !isChecked && !exitLoop; j++)
			{
				// Only continue checking if the current location is part of the king's diagonal.
				if ((i - myKingLocation.getX()) == (j - myKingLocation.getY()))
				{
					// If the current location isn't empty and isn't the source location of the move request (as it will be empty after the move)
					if (game.getBoard()->getPiece(i, j) != nullptr && !(i == moveReqSrc.getX() && j == moveReqSrc.getY()))
					{
						// Get current piece type and color.
						currentPieceType = game.getBoard()->getPiece(i, j)->getType();
						currentPieceColor = game.getBoard()->getPiece(i, j)->getColor();

						// Check if there's a threatening queen or rook in the current location.
						threateningBishopOrQueen(game, isChecked, exitLoop, currentPieceType, currentPieceColor);
					}
					// If the current location is the destination location of the piece to move
					if (i == moveReqDest.getX() && j == moveReqDest.getY())
					{
						isChecked = false;
						exitLoop = true; // The piece will be protecting the king, so exit the loop.
					}
				}
			}

		}
	}

	exitLoop = isChecked; // If king is checked, there's no need to keep checking.

	// Check second diagonal
	// Down-left (check from the place closest to the king).
	// Only check if king's location isn't the bottom-left corner.
	if (!(myKingLocation.getX() == 0 && myKingLocation.getY() == CORNER))
	{
		for (i = myKingLocation.getX() - 1; i >= 0 && !isChecked && !exitLoop; i--)
		{
			for (j = myKingLocation.getY() + 1; j < ROWS && !isChecked && !exitLoop; j++)
			{
				// Only continue checking if the current location is part of the king's diagonal.
				if ((myKingLocation.getX() - i) == (j - myKingLocation.getY()))
				{
					// If the current location isn't empty and isn't the source location of the move request (as it will be empty after the move)
					if (game.getBoard()->getPiece(i, j) != nullptr && !(i == moveReqSrc.getX() && j == moveReqSrc.getY()))
					{
						// Get current piece type and color.
						currentPieceType = game.getBoard()->getPiece(i, j)->getType();
						currentPieceColor = game.getBoard()->getPiece(i, j)->getColor();

						// Check if there's a threatening queen or rook in the current location.
						threateningBishopOrQueen(game, isChecked, exitLoop, currentPieceType, currentPieceColor);
					}
					// If the current location is the destination location of the piece to move
					if (i == moveReqDest.getX() && j == moveReqDest.getY())
					{
						isChecked = false;
						exitLoop = true; // The piece will be protecting the king, so exit the loop.
					}
				}
			}
		}
	}

	exitLoop = isChecked; // If king is checked, there's no need to keep checking.

	// Up-right (check from the place closest to the king).
	// Only check if king's location isn't the top-right corner.
	if (!(myKingLocation.getX() == CORNER && myKingLocation.getY() == 0))
	{
		for (i = myKingLocation.getX() + 1; i < COLS && !isChecked && !exitLoop; i++)
		{
			for (j = myKingLocation.getY() - 1; j >= 0 && !isChecked && !exitLoop; j--)
			{
				// Only continue checking if the current location is part of the king's diagonal.
				if ((i - myKingLocation.getX()) == (myKingLocation.getY() - j))
				{
					// If the current location isn't empty and isn't the source location of the move request (as it will be empty after the move)
					if (game.getBoard()->getPiece(i, j) != nullptr && !(i == moveReqSrc.getX() && j == moveReqSrc.getY()))
					{
						// Get current piece type and color.
						currentPieceType = game.getBoard()->getPiece(i, j)->getType();
						currentPieceColor = game.getBoard()->getPiece(i, j)->getColor();

						// Check if there's a threatening queen or rook in the current location.
						threateningBishopOrQueen(game, isChecked, exitLoop, currentPieceType, currentPieceColor);
					}
					// If the current location is the destination location of the piece to move
					if (i == moveReqDest.getX() && j == moveReqDest.getY())
					{
						isChecked = false;
						exitLoop = true; // The piece will be protecting the king, so exit the loop.
					}
				}
			}
		}
	}

	exitLoop = isChecked; // If king is checked, there's no need to keep checking.
}

/*
Function checks if the pawnLocation contains a threatening pawn.
Input: game, isChecked and pawnLocation references.
Output: none.
*/
void helper::threateningPawn(chessGame& game, bool& isChecked, location& pawnLocation)
{
	char pieceType = 0; 
	unsigned int pieceColor = 0;

	// If the location isn't empty and also isn't the destination of the current move request
	// (in that case - whatever piece is there will be eaten by the player).
	if (game.getBoard()->getPiece(pawnLocation) != nullptr && pawnLocation.getX() != game.getMoveReq().getDest().getX() && pawnLocation.getY() != game.getMoveReq().getDest().getY())
	{
		pieceType = game.getBoard()->getPiece(pawnLocation)->getType();
		pieceColor = game.getBoard()->getPiece(pawnLocation)->getColor();

		// If the piece in the location is an enemy pawn, the king is in check.
		if (pieceType == PAWN && pieceColor != game.getCurrentPlayer())
		{
			isChecked = true;
		}
	}
}

/*
Function checks the if there are any enemy pawns threatening the king.
Input: game reference, myKingLocation reference, isChecked reference.
Output: none.
*/
void helper::checkPawns(chessGame& game, location& myKingLocation, bool& isChecked)
{
	location pawnLocation;

	// If the player is black, check the two hypothetic locations of threatening pawns.
	if (game.getCurrentPlayer() == BLACK)
	{
		pawnLocation.setLocation(myKingLocation.getX() - 1, myKingLocation.getY() + 1);
		threateningPawn(game, isChecked, pawnLocation);

		// Check if the piece moving is going to eat the current pawn.
		if (game.getMoveReq().getDest().getX() == pawnLocation.getX() && game.getMoveReq().getDest().getY() == pawnLocation.getY())
		{
			isChecked = false; // The piece moving will eat the current pawn, so king isn't in check by this pawn.
		}

		// If the king isn't in check, check the other location too.
		if (!isChecked)
		{
			pawnLocation.setLocation(myKingLocation.getX() + 1, myKingLocation.getY() + 1);
			threateningPawn(game, isChecked, pawnLocation);

			// Check if the piece moving is going to eat the current pawn.
			if (game.getMoveReq().getDest().getX() == pawnLocation.getX() && game.getMoveReq().getDest().getY() == pawnLocation.getY())
			{
				isChecked = false; // The piece moving will eat the current pawn, so king isn't in check by this pawn.
			}
		}
	}
	else // If the player is white, check the two hypothetic locations of threatening pawns.
	{
		pawnLocation.setLocation(myKingLocation.getX() - 1, myKingLocation.getY() - 1);
		threateningPawn(game, isChecked, pawnLocation);

		// Check if the piece moving is going to eat the current pawn.
		if (game.getMoveReq().getDest().getX() == pawnLocation.getX() && game.getMoveReq().getDest().getY() == pawnLocation.getY())
		{
			isChecked = false; // The piece moving will eat the current pawn, so king isn't in check by this pawn.
		}

		// If the king isn't in check, check the other location too.
		if (!isChecked)
		{
			pawnLocation.setLocation(myKingLocation.getX() + 1, myKingLocation.getY() - 1);
			threateningPawn(game, isChecked, pawnLocation);

			// Check if the piece moving is going to eat the current pawn.
			if (game.getMoveReq().getDest().getX() == pawnLocation.getX() && game.getMoveReq().getDest().getY() == pawnLocation.getY())
			{
				isChecked = false; // The piece moving will eat the current pawn, so king isn't in check by this pawn.
			}
		}
	}
}

/*
Function checks if the enemy king can move to the current player's king.
Input: game reference, myKingLocation reference, isChecked reference.
Output: none.
*/
void helper::checkEnemyKing(chessGame& game, location& myKingLocation, bool& isChecked)
{
	location enemyKingLocation = findEnemyKing(game);

	// If the enemy king can move to the current player's king, the current player's king is in check.
	isChecked = game.getBoard()->getPiece(enemyKingLocation)->canMoveToKing(enemyKingLocation, myKingLocation, game);
}

/*
Function checks if one of the enemy's knights can move to the current player's king.
Input: game reference, myKingLocation reference, isChecked reference.
Output: none.
*/
void helper::checkKnights(chessGame& game, location& myKingLocation, bool& isChecked)
{
	unsigned int i = 0, j = 0, currentPieceColor = 0, counter = 0;
	char currentPieceType = 0;
	location enemyKnightLocation;

	// Search the board for enemy knights until a threatening one is found
	// or until there have been 2 enemy knights found (which is the maximum possible number of knights).
	for (i = 0; i < COLS && !isChecked && counter < MAX_NUMBER_OF_KNIGHTS; i++)
	{
		for (j = 0; j < ROWS && !isChecked && counter < MAX_NUMBER_OF_KNIGHTS; j++)
		{
			// If the current location isn't empty
			if (game.getBoard()->getPiece(i, j) != nullptr)
			{
				currentPieceColor = game.getBoard()->getPiece(i, j)->getColor();
				currentPieceType = game.getBoard()->getPiece(i, j)->getType();

				// If the current piece is an enemy knight
				if (currentPieceColor != game.getCurrentPlayer() && currentPieceType == KNIGHT)
				{
					counter++; // Count the number of enemy knights found.
					enemyKnightLocation.setLocation(i, j); // Set the enemy knight's location.

					// Check if the enemy's knight can move to king. If it can, king is in check.
					isChecked = game.getBoard()->getPiece(i, j)->canMoveToKing(enemyKnightLocation, myKingLocation, game);

					// Check if the piece moving is going to eat this knight.
					if (i == game.getMoveReq().getDest().getX() && j == game.getMoveReq().getDest().getY())
					{
						isChecked = false; // The piece moving will eat the current knight, so king isn't in check by this knight.
					}
				}
			}
		}
	}
}

/*
Function checks if the move will lead the current player to be in check.
Input: game reference.
Output: none.
*/
void helper::leadsToCheck(chessGame& game)
{
	bool isChecked = false, exitLoop = false;
	location myKingLocation;
	location moveReqSrc = game.getMoveReq().getSrc();
	location moveReqDest = game.getMoveReq().getDest();

	// Change the current player to the opposite color to find its enemy king
	// (which is actually the current player's king)
	game.setCurrentPlayer();
	myKingLocation = findEnemyKing(game);
	game.setCurrentPlayer(); // Set current player back

	// If the piece moved is the current player's king
	if (myKingLocation.getX() == moveReqSrc.getX() && myKingLocation.getY() == moveReqSrc.getY())
	{
		myKingLocation.setLocation(moveReqDest.getX(), moveReqDest.getY()); // Set king's location to the location after the move.
	}

	// Check rows and columns for a threatening rook or queen.
	checkRowAndColumn(game, myKingLocation, isChecked, exitLoop);

	// Check diagonals for a threatening queen or bishop.
	checkDiagonals(game, myKingLocation, isChecked, exitLoop);

	// Check pawns
	if (!isChecked)
	{
		checkPawns(game, myKingLocation, isChecked);
	}

	// Check enemy king
	if (!isChecked)
	{
		checkEnemyKing(game, myKingLocation, isChecked);
	}

	// Check knights
	checkKnights(game, myKingLocation, isChecked);

	if (isChecked)
	{
		throw answerCodes::SELF_IS_CHECKED;
	}
}

/*
Function checks if the source and destination are the same.
Input: game reference.
Output: none.
*/
void helper::isSameLocation(chessGame& game)
{
	location srcLoc = game.getMoveReq().getSrc();
	location destLoc = game.getMoveReq().getDest();

	// If the locations are the same, throw exception.
	if (srcLoc.getX() == destLoc.getX() && srcLoc.getY() == destLoc.getY())
	{
		throw answerCodes::SRC_SAME_AS_DEST;
	}
}
