#pragma once
#include "gamePiece.h"

class gamePiece;
class chessGame;

class rook : public gamePiece
{
public:
	rook(int color, char type);
	~rook() {};

	bool condition(location& src, location& dest, chessGame& game);

private:
	bool isWayClear(location& src, location& dest, chessGame& game);
};