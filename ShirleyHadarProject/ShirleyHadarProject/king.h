#pragma once
//#include "stdafx.h"
#include "gamePiece.h"

class gamePiece;
class chessGame;

class king : public gamePiece
{
public:
	king(int color, char type);
	~king() {};

	bool condition(location& src, location& dest, chessGame& game);
};