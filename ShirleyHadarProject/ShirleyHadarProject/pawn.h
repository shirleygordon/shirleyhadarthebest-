#pragma once
#include "gamePiece.h"

class gamePiece;

class pawn : public gamePiece
{
public:
	pawn(int color, char type);
	~pawn() {};
	bool condition(location& src, location& dest, chessGame& game);

private:
	bool isFirstMove(chessGame& game);
	bool isEating(chessGame& game, location& destLoc);

};
