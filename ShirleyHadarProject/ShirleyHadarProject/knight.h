#pragma once
#include "gamePiece.h"

class gamePiece;
class chessGame;

class knight : public gamePiece
{
public:
	knight(int color, char type);
	~knight() {};

	bool condition(location& src, location& dest, chessGame& game);

};