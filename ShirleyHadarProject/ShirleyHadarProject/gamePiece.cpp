#include "gamePiece.h"

/*
C'tor for gamePiece.
Input: color, type.
Output: none.
*/
gamePiece::gamePiece(int color, char type)
{
	this->_color = color;
	this->_type = type;
}

void gamePiece::setColor(int color)
{
	this->_color = color;
}

void gamePiece::setType(char type)
{
	this->_type = type;
}

int gamePiece::getColor()
{
	return this->_color;
}

char gamePiece::getType()
{
	return this->_type;
}

/*
	recieves the game (type: chessGame)
	checks if the move is valid by checking the condition of the piece
	returns bool: true if valid or false if not valid
*/
void gamePiece::canMove(chessGame& game)
{
	location src = game.getMoveReq().getSrc();
	location dest = game.getMoveReq().getDest();

	if (!condition(src, dest, game))
	{
		throw answerCodes::INVALID_PIECE_MOVE;
	}
}

/*
	recieves the location of a piece (type: location), the location of the king (type: location) and the game (type:chessGame)
	checks if there is a hypothetical possibility of there being a check by that specific piece
	returns bool: true if there is a possibility and false if there isn't
*/
bool gamePiece::canMoveToKing(location src, location king, chessGame& game)
{
	bool res = false;

	if (condition(src, king, game))
	{
		res = true;
	}

	return res;
}
