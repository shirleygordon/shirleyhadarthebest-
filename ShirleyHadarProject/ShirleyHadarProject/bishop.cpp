#include "bishop.h"
#define NEG -1

bishop::bishop(int color, char type) : gamePiece(color, type)
{
}

/*
	recieves the location of a piece (type: location), the location of the king (type: location) and the game (type:chessGame)
	checks if the movement is possible for the specific piece from the it's source to it's requested destination
	returns bool: true if valid, false if not valid
*/
bool bishop::condition(location& src, location& dest, chessGame& game)
{
	bool res = false;

	int x_src = src.getX();
	int y_src = src.getY();
	int x_dest = dest.getX();
	int y_dest = dest.getY();

	int diff = x_src - x_dest;

	if (diff < 0)
	{
		diff * NEG;
	}

	// If way is clear
	if (isWayClear(src, dest, game))
	{
		// Right and up
		if (x_dest == x_src + diff && y_dest == y_src + diff)
		{
			res = true;
		}
		// Left and up
		else if (x_dest == x_src - diff && y_dest == y_src + diff)
		{
			res = true;
		}
		// Right and down 
		else if (x_dest == x_src + diff && y_dest == y_src - diff) 		
		{
			res = true;
		}
		// Left and down
		else if (x_dest == x_src - diff && y_dest == y_src - diff)
		{
			res = true;
		}
	}
	

	return res;
}

/*
	recieves the location of a piece (type: location), the location of the king (type: location) and the game (type:chessGame)
	checks if all the steps to the destination are clear
	returns bool: true if clear, false if not
*/
bool bishop::isWayClear(location& src, location& dest, chessGame& game)
{
	bool res = true;
	int i = 0, j = 0;

	int x_src = src.getX();
	int y_src = src.getY();
	int x_dest = dest.getX();
	int y_dest = dest.getY();

	//dest is up and right
	if (x_dest > x_src && y_dest > y_src)
	{
		for (i = x_src + 1, j = y_src + 1; i < x_dest && j < y_dest && res; i++, j++)
		{
			if (game.getBoard()->getPiece(i, j) != nullptr)
			{
				res = false;
			}
		}
	}

	//dest is down and right
	if (x_dest > x_src && y_dest < y_src)
	{
		for (i = x_src + 1, j = y_src - 1; i < x_dest && j > y_dest && res; i++, j--)
		{
			if (game.getBoard()->getPiece(i, j) != nullptr)
			{
				res = false;
			}
		}
	}

	//dest is up and left
	if (x_dest < x_src&& y_dest > y_src)
	{
		for (i = x_src - 1, j = y_src + 1; i > x_dest && j < y_dest && res; i--, j++)
		{
			if (game.getBoard()->getPiece(i, j) != nullptr)
			{
				res = false;
			}
		}
	}

	//dest is down and left
	if (x_dest < x_src && y_dest < y_src)
	{
		for (i = x_src - 1, j = y_src - 1; i > x_dest&& j > y_dest && res; i--, j--)
		{
			if (game.getBoard()->getPiece(i, j) != nullptr)
			{
				res = false;
			}
		}
	}

	return res;
}