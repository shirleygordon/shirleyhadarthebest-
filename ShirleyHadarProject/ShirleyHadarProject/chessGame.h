#pragma once
#include <iostream>
#include <string>
#include "gamePiece.h"
#include "moveRequest.h"
#include "board.h"
#include "helper.h"

using std::string;

#define INVALID_MOVE '6'
#define ROWS 8
#define COLS 8
#define BLACK 1
#define WHITE 0
#define EMPTY '#'

class board;
class gamePiece;
class moveRequest;

class chessGame
{
private:
	int _currentPlayer;
	board* _board;
	moveRequest _moveReq;

public:
	chessGame();
	~chessGame();
	
	void movePiece();
	int getCurrentPlayer() const;
	void setCurrentPlayer();
	board* getBoard();
	void updateMoveRequest(string& request);
	moveRequest getMoveReq() const;
};
