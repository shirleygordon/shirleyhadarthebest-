#pragma once
#include "gamePiece.h"

#define ROWS 8
#define COLS 8
#define WHITE_ROW 7
#define WHITE_PAWN_ROW 6
#define BLACK_ROW 0
#define BLACK_PAWN_ROW 1

enum piecesOrder {ROOK1, KNIGHT1, BISHOP1, KING, QUEEN, BISHOP2, KNIGHT2, ROOK2};
enum pieceTypes { PAWN = 'p', BISHOP = 'b', KING_CHAR = 'k', QUEEN_CHAR = 'q', KNIGHT = 'n', ROOK = 'r' };

class gamePiece;

class board
{
private:
	gamePiece* _board[ROWS][COLS];

public:
	board();
	~board();
	void updateBoard(moveRequest moveReq);
	gamePiece* getPiece(location srcLoc);
	gamePiece* getPiece(int x, int y);
	void setPiece(location srcLoc, gamePiece* newPiece);
};