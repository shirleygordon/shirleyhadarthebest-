#pragma once
#include "bishop.h"
#include "rook.h"

class rook;
class bishop;
class chessGame;

class queen : public gamePiece
{
public:
	queen(int color, char type);
	~queen() {};

	bool condition(location& src, location& dest, chessGame& game);
};