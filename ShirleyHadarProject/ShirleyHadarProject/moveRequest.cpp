#include "moveRequest.h"

/*
Default moveRequest C'tor.
Input: none.
Output: none.
*/
moveRequest::moveRequest() : _srcLocation(), _destLocation()
{
	this->_answer = "0";
	// Set source and destination locations to 0,0
	//this->setSrcLocation("a1");
	//this->setDestLocation("a1");
}

/*
moveRequest C'tor.
Input: request string.
Output: none.
*/
moveRequest::moveRequest(string request)
{
	this->_answer = "0";
	this->setSrcLocation(request);
	this->setDestLocation(request);
}

/*
moveRequest D'tor.
Input: none.
Output: none.
*/
moveRequest::~moveRequest()
{
}

location moveRequest::getSrc() const
{
	return this->_srcLocation;
}

location moveRequest::getDest() const
{
	return this->_destLocation;
}

string moveRequest::getAnswer() const
{
	return this->_answer;
}

/*
Function sets source location x and y.
Input: const reference to request string.
Output: none.
*/
void moveRequest::setSrcLocation(const string& request)
{
	char x = 0, y = 0;

	// Extract source x and y from request string.
	x = request[SRC_X];
	y = request[SRC_Y];

	// Set source location x and y.
	this->_srcLocation.setX(x);
	this->_srcLocation.setY(y);
}

/*
Function sets destination location x and y.
Input: const reference to request string.
Output: none.
*/
void moveRequest::setDestLocation(const string& request)
{
	char x = 0, y = 0;

	// Extract destination x and y from request string.
	x = request[DEST_X];
	y = request[DEST_Y];

	// Set destination location x and y.
	this->_destLocation.setX(x);
	this->_destLocation.setY(y);
}

/*
Function sets answer to current move request, according to the code passed to it.
Input: answer code.
Output: none.
*/
void moveRequest::setAnswer(char code)
{
	this->_answer[0] = code; // First element in answer string is the answer code.
}




