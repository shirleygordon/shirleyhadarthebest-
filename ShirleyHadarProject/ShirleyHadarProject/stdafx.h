// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include "chessGame.h"
#include "gamePiece.h"
#include "helper.h"
#include "location.h"
#include "moveRequest.h"
#include "bishop.h"
#include "king.h"
#include "knight.h"
#include "pawn.h"
#include "queen.h"
#include "rook.h"
#include "Pipe.h"


// TODO: reference additional headers your program requires here
