#include "pawn.h"

#define	ONE_STEP 1
#define	TWO_STEPS 2
#define WHITE_FIRST 6
#define BLACK_FIRST 1
#define ROW 8
#define COL 8

pawn::pawn(int color, char type) : gamePiece(color, type)
{
}

/*
	recieves thegame (type: chessGame)
	checks if it's the first move of the pawn piece
	returns bool: true if it's the first move, false if not
*/
bool pawn::isFirstMove(chessGame& game)
{
	bool res = false;
	location srcLoc = game.getMoveReq().getSrc();
	int y_src = srcLoc.getY();

	// The current player is white and the pawn is in the 7th row.
	if (y_src == WHITE_FIRST && !game.getCurrentPlayer())     
	{
		res = true;
	}
	// The current player is black and the pawn is in the 2nd row.
	else if (y_src == BLACK_FIRST && game.getCurrentPlayer())
	{
		res = true;
	}
	return res;
}

/*
	recieves thegame (type: chessGame)
	checks if the pawn is eating the oponent
	returns bool: true if the pawn is eating the oponent in the current move, false if not
*/
bool pawn::isEating(chessGame& game, location& destLoc)
{
	bool res = false;
	int x_dest = destLoc.getX();
	int y_dest = destLoc.getY();

	if (!game.getCurrentPlayer())	//white
	{
		if (game.getBoard()->getPiece(x_dest, y_dest) != nullptr &&          // the destination is not empty
			game.getBoard()->getPiece(x_dest, y_dest)->getColor() == BLACK)  // and has the oponents piece
		{
			res = true;
		}
	}

	else // Black
	{
		if (game.getBoard()->getPiece(x_dest, y_dest) != nullptr &&          // the destination is not empty
			game.getBoard()->getPiece(x_dest, y_dest)->getColor() == WHITE)  // and has the oponents piece
		{
			res = true;
		}
	}

	return res;
}

/*
	recieves the location of a piece (type: location), the location of the king (type: location) and the game (type:chessGame)
	checks if the movement is possible for the specific piece from the it's source to it's requested destination
	returns bool: true if valid, false if not valid
*/
bool pawn::condition(location& src, location& dest, chessGame& game)
{
	bool res = false;

	int x_src = src.getX();
	int y_src = src.getY();
	int x_dest = dest.getX();
	int y_dest = dest.getY();

	if (!game.getCurrentPlayer()) // Player is white
	{
		// The player took 1 step, or it was the first move and the player took 2 steps,
		// and isn't eating the opponent.
		if ((y_dest == y_src - ONE_STEP || (isFirstMove(game) && y_dest == y_src - TWO_STEPS)) && (x_src == x_dest) && !isEating(game, dest))
		{
			res = true;
		}
		// The player went sideways right or left, and is eating the opponent.
		else if (y_dest == y_src - ONE_STEP && ((x_dest == x_src - ONE_STEP) || (x_dest == x_src + ONE_STEP)) && isEating(game, dest))
		{
			res = true;
		}
	}

	else // Player is black
	{
		// The player took 1 step, or it was the first move and the player took 2 steps,
		// and isn't eating the opponent.
		if ((y_dest == y_src + ONE_STEP	|| (isFirstMove(game) && y_dest == y_src + TWO_STEPS)) && (x_src == x_dest) && !isEating(game, dest))												
		{
			res = true;
		}
		// The player went sideways right or left, and is eating the opponent.
		else if (y_dest == y_src + ONE_STEP && ((x_dest == x_src - ONE_STEP) || (x_dest == x_src + ONE_STEP)) && isEating(game, dest))												
		{
			res = true;
		}
	}

	return res;
}