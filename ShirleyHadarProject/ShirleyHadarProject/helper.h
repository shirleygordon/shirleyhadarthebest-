#pragma once
#include "chessGame.h"

#define MIN_LCASE_ASCII 97
#define MAX_LCASE_ASCII 122
#define MIN_UCASE_ASCII 65
#define MAX_UCASE_ASCII 90
#define CORNER 7
#define MAX_NUMBER_OF_KNIGHTS 2

class chessGame;
class location;
class gamePiece;
class bishop;
class king;
class knight;
class pawn;
class queen;
class rook;

enum class answerCodes : char { ENEMY_CHECKED = '1', NO_PLAYER_IN_SRC, PLAYER_IN_DEST, SELF_IS_CHECKED, OUT_OF_BOUNDS, INVALID_PIECE_MOVE, SRC_SAME_AS_DEST, CHECKMATE };

class helper
{
private:
	static location findEnemyKing(chessGame& game);
	static void threateningQueenOrRook(chessGame& game, bool& isChecked, bool& exitLoop, char& currentPieceType, unsigned int& currentPieceColor);
	static void checkRowAndColumn(chessGame& game, location& myKingLocation, bool& isChecked, bool& exitLoop);
	static void threateningBishopOrQueen(chessGame& game, bool& isChecked, bool& exitLoop, char& currentPieceType, unsigned int& currentPieceColor);
	static void checkDiagonals(chessGame& game, location& myKingLocation, bool& isChecked, bool& exitLoop);
	static void threateningPawn(chessGame& game, bool& isChecked, location& pawnLocation);
	static void checkPawns(chessGame& game, location& myKingLocation, bool& isChecked);
	static void checkEnemyKing(chessGame& game, location& myKingLocation, bool& isChecked);
	static void checkKnights(chessGame& game, location& myKingLocation, bool& isChecked);

public:
	static void isOutOfBounds(chessGame& game);
	static void isPlayerInSrcLocation(chessGame& game);
	static void isPlayerInDestLocation(chessGame& game);
	static void leadsToCheck(chessGame& game);
	static void isEnemyChecked(chessGame& game);
	static void isSameLocation(chessGame& game);
};
