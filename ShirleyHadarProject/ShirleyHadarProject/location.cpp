#include "location.h"

/*
Default location C'tor.
Input: none.
Output: none.
*/
location::location()
{
	this->_x = 0;
	this->_y = 0;
}

/*
Location D'tor.
Input: none.
Output: none.
*/
location::~location()
{
}

unsigned int location::getX()
{
	return this->_x;
}

unsigned int location::getY()
{
	return this->_y;
}

/*
Function sets the x coordinate of the location.
Input: char representing x.
Output: none.
*/
void location::setX(char x)
{
	this->_x = x - LETTER_TO_NUM;
}

/*
Function sets the y coordinate of the location.
Input: char representing y.
Output: none.
*/
void location::setY(char y)
{
	this->_y = COLS - (y - ASCII_TO_INT);
}

/*
Function sets the coordinates of the location.
Input: 2 integers representing the x and y coordinates.
Output: none.
*/
void location::setLocation(unsigned int x, unsigned int y)
{
	this->_x = x;
	this->_y = y;
}