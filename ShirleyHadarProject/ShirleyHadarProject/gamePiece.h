#pragma once
#include "chessGame.h"

class location;
class chessGame;

class gamePiece
{
protected:
	int _color;
	char _type;

public:
	gamePiece(int color, char type);
	~gamePiece() {};

	void canMove(chessGame& game);
	bool canMoveToKing(location src, location king, chessGame& game);
	virtual bool condition(location& src, location& dest, chessGame& game) = 0;

	void setColor(int color);
	void setType(char type);

	int getColor();
	char getType();

private:
};
