#pragma once

#include <iostream>
#include <string>
#include "location.h"

using std::string;

enum requestStrIndexes {SRC_X, SRC_Y, DEST_X, DEST_Y};

class moveRequest
{
private:
	location _srcLocation;
	location _destLocation;
	string _answer;

public:
	moveRequest();
	moveRequest(string request);
	~moveRequest();

	location getSrc() const;
	location getDest() const;
	string getAnswer() const;
	void setSrcLocation(const string& request);
	void setDestLocation(const string& request);
	void setAnswer(char code);
};
