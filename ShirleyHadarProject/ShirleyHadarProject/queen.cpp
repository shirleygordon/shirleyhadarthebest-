#include "queen.h"

queen::queen(int color, char type) : gamePiece(color, type)
{
}

/*
	recieves the location of a piece (type: location), the location of the king (type: location) and the game (type:chessGame)
	checks if the movement is possible for the specific piece from the it's source to it's requested destination
	returns bool: true if valid, false if not valid
*/
bool queen::condition(location& src, location& dest, chessGame& game)
{
	rook testRook(this->_color, ROOK);
	bishop testBishop(this->_color, ROOK);
	bool res = false;

	if (testRook.condition(src, dest, game) || testBishop.condition(src, dest, game))
	{
		res = true;
	}

	return res;
}