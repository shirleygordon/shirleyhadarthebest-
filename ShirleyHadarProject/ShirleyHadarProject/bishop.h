#pragma once
#include "gamePiece.h"

class gamePiece;
class chessGame;

class bishop : public gamePiece
{
public:
	bishop(int color, char type);
	~bishop() {};

	bool condition(location& src, location& dest, chessGame& game);

private:
	bool isWayClear(location& src, location& dest, chessGame& game);

};