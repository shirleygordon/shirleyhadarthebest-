#include "chessGame.h"

/*
Function sets the current player to either white or black.
Input: none.
Output: none.
*/
void chessGame::setCurrentPlayer()
{
	// If player was black, set player to white and vice versa.
	if (this->_currentPlayer == WHITE)
	{
		this->_currentPlayer = BLACK;
	}
	else
	{
		this->_currentPlayer = WHITE;
	}
}

/*
Default chessGame C'tor.
Input: none.
Output: none.
*/
chessGame::chessGame() : _moveReq()
{
	this->_currentPlayer = BLACK; // First player is always black.
	_board = new board();
}

/*
chessGame D'tor.
Input: none.
Output: none.
*/
chessGame::~chessGame()
{
	delete _board;
}

/*
Function moves a chess piece according to user's request if the request is valid.
Input: none.
Output: none.
*/
void chessGame::movePiece()
{
	gamePiece* pieceToMove = this->_board->getPiece(this->_moveReq.getSrc());
	bool validMove = false;

	// Check all the possible errors.
	try
	{
		// Check if the source or destination are out of bounds.
		helper::isOutOfBounds(*this);

		// Check if the source and destination are the same.
		helper::isSameLocation(*this);

		// Check if the source location contains a chess piece belonging to the current player.
		helper::isPlayerInSrcLocation(*this); 

		// Check if the destination location contains a chess piece belonging to the current player.
		helper::isPlayerInDestLocation(*this);

		// Check if piece can move according to its rules.
		pieceToMove->canMove(*this); 

		validMove = true; // If there were no exceptions, the move is valid.
	}
	catch (answerCodes& errorCode) // If an error occured, error code will be thrown.
	{
		this->_moveReq.setAnswer((char)errorCode); // Put error code in answer
	}
	
	if (validMove)
	{
		try
		{
			// If the move is valid, check if it leads the current player to be in check.
			helper::leadsToCheck(*this);
		}
		catch (answerCodes& errorCode)
		{
			this->_moveReq.setAnswer((char)errorCode); // Put error code in answer
			validMove = false;
		}
	}
	
	if (validMove)
	{
		try
		{
			_board->updateBoard(this->_moveReq); // Update the board
			helper::isEnemyChecked(*this); // Check if the move led the enemy to be checked.
		}
		catch (answerCodes& answerCode) // If enemy is in check or checkmated, change answer.
		{
			this->_moveReq.setAnswer((char)answerCode); // Put answer code in answer
		}

		this->setCurrentPlayer(); // Switch player if move is valid
	}
}

int chessGame::getCurrentPlayer() const
{
	return this->_currentPlayer;
}

board* chessGame::getBoard()
{
	return this->_board;
}

/*
Function updates the move request with a new request.
Input: new request string.
Output: none.
*/
void chessGame::updateMoveRequest(string& request)
{
	this->_moveReq.setAnswer('0'); // Default answer code
	this->_moveReq.setSrcLocation(request);
	this->_moveReq.setDestLocation(request);	
}

moveRequest chessGame::getMoveReq() const
{
	return this->_moveReq;
}